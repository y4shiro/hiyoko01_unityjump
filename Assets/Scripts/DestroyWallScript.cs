﻿using UnityEngine;
using System.Collections;

public class DestroyWallScript : MonoBehaviour {

	GameObject mainCam;

	void Start () {
		mainCam = GameObject.FindWithTag("MainCamera");
	}

	void Update () {
		if(transform.position.y <= mainCam.transform.position.y - 20){
			Destroy(this.gameObject);
		}
	}
}
