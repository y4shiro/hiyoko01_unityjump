﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameOverScript : MonoBehaviour {

	bool gameOverFlg = false;

	void Start () {

		// GameOverの文字を非表示
		gameObject.GetComponent<Text>().enabled = false;
	}

	void Update () {

		// Loseメソッドが呼び出され、画面をクリックした時、タイトル画面へ戻る
		if(gameOverFlg == true && Input.GetMouseButtonDown(0)){

			Application.LoadLevel("Title");
		}
	}

	public void Lose(){

		// GameOverの文字を表示
		gameObject.GetComponent<Text>().enabled = true;
		gameOverFlg = true;
	}
}
