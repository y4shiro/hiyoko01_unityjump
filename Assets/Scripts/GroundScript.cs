﻿using UnityEngine;
using System.Collections;

public class GroundScript : MonoBehaviour {
	
	bool setOff;
	BoxCollider2D colliderOfGround;
	GameObject mainCam;

	void Start () {
		colliderOfGround = GetComponent<BoxCollider2D>();
		// MainCameraを取得しておく
		mainCam = GameObject.FindWithTag("MainCamera");
	}

	void Update () {

		if(setOff){
			colliderOfGround.enabled = false;
		}
		if(!setOff){
			colliderOfGround.enabled = true;
		}

		// MainCameraの高さから6引いた位置より下に来たら、足場をDestroy
		if(transform.position.y <= mainCam.transform.position.y - 6){
			Destroy(gameObject);
		}
	}

	void OnTriggerStay2D(Collider2D col){
		if(col.gameObject.tag == "Player"){
			setOff = true;
		}
	}
	void OnTriggerExit2D(Collider2D col){
		if(col.gameObject.tag == "Player"){
			setOff = false;
		}
	}
}
