﻿using UnityEngine;
using System.Collections;

public class PlayerScript : MonoBehaviour {

	Rigidbody2D rb;
	Animator anim;
	public int moveSpeed = 2;
	public LayerMask groundLayer; // 地面のレイヤー
	public bool isGrounded; // 着地しているかの判定

	void Start () {
		// GetComponentの処理をキャッシュしておく
		rb = GetComponent<Rigidbody2D>();
		// Animatorをキャッシュ
		anim = GetComponent<Animator>();
	}


	// FixedUpdateだとクリックを判定できない場合があるため、
	// ジャンプの処理はUpdateメソッドに記述

	void Update(){

		rb.velocity = new Vector2(transform.localScale.x * moveSpeed, rb.velocity.y);

		// ユニティちゃん中央から足元にかけて、設置判定用のラインを引く
		isGrounded = Physics2D.Linecast(
			transform.position + transform.up * 1,
			transform.position - transform.up * 0.1f,
			groundLayer
			); // Linecastが判定するレイヤー

		Anim();
	}

	void FixedUpdate () {
		// localScale.xには1か-1が入る
		rb.velocity = new Vector2(transform.localScale.x * moveSpeed, rb.velocity.y);
	}

	void OnCollisionEnter2D(Collision2D col){

		if(col.gameObject.tag == "Wall"){ // 2Dの衝突判定
			// ユニティちゃんのlocalScaleを変数に格納
			Vector2 temp = gameObject.transform.localScale;
			// localScale.xに-1をかける
			temp.x *= -1;
			// 結果を返す
			gameObject.transform.localScale = temp;
		}
	}

	public void Jump(int jumpForce){

		// ジャンプアニメーションの開始
		anim.SetTrigger("jump");
		// 上方向へ力を加える
		rb.AddForce(Vector2.up * jumpForce);
		// 地面が離れるのでfalseにする
		isGrounded = false;
	}

	void Anim(){

		// velY:y方向へかかる速度単位、上へ行くとプラス、下へ行くとマイナス
		float velY = rb.velocity.y;
		// Animatorへパラメータを送る
		anim.SetFloat("velY", velY);
		anim.SetBool("isGrounded", isGrounded);
	}
}
