﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BarScript : MonoBehaviour {

	public PlayerScript playerScript;
	RectTransform rt;

	void Start () {

		// bar1のRectTransformコンポーネントをキャッシュ
		rt = GetComponent<RectTransform>();
	}

	void Update () {

		// クリック中かつ、PlayerScriptのisGroundedがtrueであるときゲージを増やす
		if(Input.GetMouseButton(0) && playerScript.isGrounded){

			// sizeDeltaでゲージの大きさを制御
			// 左端から、毎フレーム4ずつ増やす
			rt.sizeDelta = new Vector2(rt.sizeDelta.x + 4.0f, 50.0f);
			// ゲージの横幅が300を超えたら0に戻る
			if(rt.sizeDelta.x >= 300){

				rt.sizeDelta = new Vector2(0.0f, 50.0f);
			}
		}

		// クリックを話した時
		if(Input.GetMouseButtonUp(0)){

			// unityChanControllerのGetJumpForceメソッドへ、ゲージ横幅の3杯分のjumpForceを送る
			playerScript.SendMessage("Jump", rt.sizeDelta.x * 2.5);
			// ジャンプしたらジャンプ力ゲージを0に戻す
			rt.sizeDelta = new Vector2 (0.0f, 50.0f);
		}
		if(playerScript.isGrounded == false){

			// 地面から落ちたらジャンプ力ゲージを0に戻す
			rt.sizeDelta = new Vector2(0.0f, 05.0f);
		}
	}
}
