﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CameraScript : MonoBehaviour {
	
	public GameObject player;
	private Transform playerTrans;
	public Text scoreText; // Text用変数
	public Text highScoreText; // ハイスコアを表示するTextオブジェクト
	private int score = 0; // スコア計算用変数
	private int scoreUpPos = 3;
	public GameOverScript gameOverScript;

	private int highScore;
	private string key = "HIGH SCORE";

	public GameObject step;
	public GameObject wall;
	private int createWallPos = 3;

	void Start (){
		//プレイヤーの位置情報をキャッシュ
		playerTrans = player.GetComponent<Transform>();
		scoreText.text = "Score: 0"; //初期スコアを代入して画面に表示

		// 保存しておいたハイスコアをキーで呼び出し取得
		// 保存されていなければ0が返る
		highScore = PlayerPrefs.GetInt(key, 0);
		// ハイスコアを表示
		highScoreText.text = "HighScore: " + highScore.ToString();
	}
	
	void Update (){
		//プレイヤーの高さ（y軸）を取得
		float playerHeight = playerTrans.position.y;
		//現在のカメラの高さ（y軸）を取得
		float currentCameraHeight = transform.position.y;
		//Lerp(from,to,割合)最小値と最大値の間の値を取る。
		//第３引数割合に0.0〜1.0の値を入れ、値を決定する。0.5なら調度真ん中の値を取得する。
		float newHeight = Mathf.Lerp (currentCameraHeight, playerHeight, 0.5f);
		//現在のカメラの高さよりプレイヤーの高さのほうが高くなったら、カメラの高さをnewHeightにする。
		//x軸とz軸は変更しない。
		if (playerHeight > currentCameraHeight) {
			transform.position = new Vector3 (transform.position.x, newHeight, transform.position.z);
		}
		// scoreUpPosを超えた時
		if(playerTrans.position.y >= scoreUpPos){

			scoreUpPos += 3; // scoreUpPosを高くする
			score += 10; // scoreに加算
			scoreText.text = "Score: " + score.ToString();

			// ハイスコアより現在スコアが高い場合
			if(score > highScore){

				// ハイスコア更新
				highScore = score;
				// ハイスコアを保存
				PlayerPrefs.SetInt(key, highScore);
				// ハイスコアを表示
				highScoreText.text = "HighScore: " + highScore.ToString();
			}
			CreateStep();
		}

		if (playerTrans.position.y > createWallPos){
			CreateWall();
			createWallPos += 10;
		}

		// ユニティちゃんの位置がカメラの位置より-6低くなった場合
		if(playerTrans.position.y <= currentCameraHeight - 6){

			// gameOverScriptのLoseメソッドを実行
			gameOverScript.Lose();
			// ユニティちゃんを削除
			Destroy(player);
		}
	}

	void CreateStep(){
		Instantiate (step, new Vector2 (Random.Range (-6.0f, 0f), scoreUpPos + Random.Range (4.0f, 6.0f)), step.transform.rotation);
		Instantiate (step, new Vector2 (Random.Range (0f, 6.0f), scoreUpPos + Random.Range (4.0f, 6.0f)), step.transform.rotation);
	}

	void CreateWall(){
		Instantiate (wall, new Vector2 (10f, createWallPos + 10), wall.transform.rotation);
		Instantiate (wall, new Vector2 (-10f, createWallPos + 10), wall.transform.rotation);
	}
}